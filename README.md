# Sentinel Comics The Roleplaying Game (Unofficial)

This game system for [Foundry Virtual Tabletop](https://foundryvtt.com/) provides the character sheets and game system for [Sentinel Comics the Roleplaying Game](https://greaterthangames.com/product/sentinel-comics-the-role-playing-game-core-rulebook/) by [Greater than Games](https://greaterthangames.com/).

This system provides character sheet support for Heroes, Villains and Environments. It also provides macros for updating all hero character's scene status.

This is an unofficial package and a copy of the rulebook will be required to use this system.

## Features

- Support for Heroes, Villain and Environment character sheets
- Support for Scene Tracker and Initiative
    - Scene Tracker automatically updates all hero character sheet scene setting when the scene changes
- Support for Modular, Form-Changer, Divided, Minion-Maker archetypes
    - Modular characters can choose to have a non-powered form
    - Divided psyche (rolling two powers/two qualities) and divided status can both be enabled separately
    - Different modes/forms of Modular and Form-Changer characters can have different powers and power die
    - Forms/Modes abilities, powers and description can be filled out on the Auxiliary tab but can also be viewed on the Power tab
    - Form/Mode can be changed on the Auxiliary tab or from the drop-down menu on the Power tab
    - Minion forms and minions can be tacked on minion tab
- Hero heath ranges are automatically calculated from entering Max health
- Hero Status is automatically determined from heath range and scene status
- Powers, qualities, and abilities can be added directly on the character sheet
- Power, quality and status die can be rolled individually
- Power, quality and status die can be rolled together
    - Chat will display the dice in order of Max, Mid and Min results
- Villains can add Statuses to their sheet
- Macros for updating all hero character's scene status (found in the compendium)
- Track hero points, hero reward points, back issues, and collections on the Hero tab
- Track Bonus and Penalties
    - Penalties will make dice roll chat messages red as a reminder
- Background, Power Source, Archetype, Personality, and Principles items can be dragged onto the the character sheet
- Abilites, Environment Twists, and Minion Forms can be submitted to chat
- Items on character sheets can be edited and deleted by right clicking on them

## Game Settings

The following can be enabled in the game settings
- Colored dice in the chat window
- Unavailable abilities will grey out depending on your status
- Bonus and Penalty tracking on Power tab

## Added by DaddyDM

- Abilities have a roll button if appropriate so they can be rolled without having to scroll up to top of the sheet
- Abilities (and minions) can be dragged to the hotbar so you don't even need the sheet
- Using the new roll buttons for 3-dice rolls will bring up new dialog that smartly picks power, quality, and/or status depending on the ability text, and also lets the user select/deselect bonuses and penalties.
- When abilities are rolled, the ability text is printed to the chat above the roll
- When minions/lieutenants are rolled, their abilities are printed to the chat above the roll
- Powers and abilities can be dropped on forms/modes on the Auxiliary tab
- Added an "Add All" button to the Scene sheet that adds all tokens in the current scene to the tracker
- Fixed the "Reset" button on the Scene sheet; it now properly sends all actors back to the green column
- TODO: Adding Upgrades and Masteries to villains, and auto-calculating villain health based on the number of heroes
