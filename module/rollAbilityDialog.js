import * as dice from "./dice.js";

/**
 * A dialog for interactive task rolls
 */

export class RollAbilityDialog extends FormApplication {

    static get defaultOptions(){
        return mergeObject(super.defaultOptions, {
            template: "systems/scrpg/templates/sheets/rollAbility-sheet.hbs",
            classes: ["SCRPG", "sheet", "item"],
            resizable: true
        });
    }

    constructor(item) {
        super();
        this.ability = item;
        this.actor = this.ability.actor;
        this.options.title = this.ability.name;
    }

    getData() {
        const data = super.getData();
        data.config = CONFIG.SCRPG;
        
        data.ability = this.ability;
        data.actor = this.actor;
        data.powers = this._getPowers();
        data.qualities = this._getQualities();
        data.statuses = this._getStatuses();
        data.bonuses = this.actor.items.filter(function(item) {
            return item.type === "mod" && parseInt(item.system.value) > 0;
        });
        data.penalties = this.actor.items.filter(function(item) {
            return item.type === "mod" && parseInt(item.system.value) < 0;
        });
        data.hasAvailMods = data.bonuses.length > 0 || data.penalties.length > 0;
        
        data.selectedPowerId = null;
        data.selectedQualityId = null;
        data.selectedStatusId = null;
        
        data.selectedPower = null;
        data.selectedQuality = null;
        data.selectedStatus = null;

        data.powerLocked = false;
        data.qualityLocked = false;
        data.statusLocked = (this.actor.type === "hero");

        data.rollPower = true;
        data.rollQuality = true;
        data.rollStatus = true;

        this._setSelectedItems(data);
        
        this.data = data;
        return data;
    }
    
    _getPowers() {
        let powers = [];
        this.actor.items.forEach(function(item) {
            if (item.type === "power") {
                powers.push({
                    id: item._id,
                    name: item.name,
                    dieType: item.system.dieType,
                    selected: (item.system.selected ? true : false),
                    aux: item.system.aux
                });
            }
        });
        powers.push({
            id: "N/A",
            name: "N/A",
            dieType: "d4",
            selected: false,
            aux: ""
        });
        return powers;
    }
    _getQualities() {
        let qualities = [];
        this.actor.items.forEach(function(item) {
            if (item.type === "quality") {
                qualities.push({
                    id: item._id,
                    name: item.name,
                    dieType: item.system.dieType,
                    selected: (item.system.selected ? true : false)
                });
            }
        });
        qualities.push({
            id: "N/A",
            name: "N/A",
            dieType: "d4",
            selected: false
        });
        return qualities;
    }
    _getStatuses() {
        let statuses = [];
        if (this.actor.type === "villain") {
            this.actor.items.forEach(function(item) {
                if (item.type === "villainStatus") {
                    statuses.push({
                        id: item._id,
                        name: item.name,
                        dieType: item.system.dieType,
                        selected: (item.system.selected ? true : false)
                    });
                }
            });
        }
        else if (this.actor.type === "hero") {
            statuses.push({
                id: "1",
                name: "Green",
                dieType: this.actor.system.statusDie.green,
                selected: (this.actor.system.thirdDieName === "green" ? true : false)
            });
            statuses.push({
                id: "2",
                name: "Yellow",
                dieType: this.actor.system.statusDie.yellow,
                selected: (this.actor.system.thirdDieName === "yellow" ? true : false)
            });
            statuses.push({
                id: "3",
                name: "Red",
                dieType: this.actor.system.statusDie.red,
                selected: (this.actor.system.thirdDieName === "red" ? true : false)
            });
        }
        // else ???
        return statuses;
    }    
    _setSelectedItems(data) {
        // see if any powers or qualities or statuses are in ability text
        let abilityText = this.ability.system.gameText;
        let foundNamedItem = false;
        let itemMatch = data.powers.find(function(item) {
            return abilityText.includes(item.name);
        });
        if (itemMatch) {
            this._setSelectedInArray(data.powers, itemMatch.id);
            data.selectedPowerId = itemMatch.id;
            data.selectedPower = itemMatch;
            data.powerLocked = true;
            foundNamedItem = true;
            if (this.ability.system.type !== "A") {
                data.rollQuality = false;
                data.rollStatus = false;
            }
        }
        if (!foundNamedItem) {
            itemMatch = data.qualities.find(function(item) {
                return abilityText.includes(item.name);
            });
            if (itemMatch) {
                this._setSelectedInArray(data.qualities, itemMatch.id);
                data.selectedQualityId = itemMatch.id;
                data.selectedQuality = itemMatch;
                data.qualityLocked = true;
                foundNamedItem = true;
                if (this.ability.system.type !== "A") {
                    data.rollPower = false;
                    data.rollStatus = false;
                }
            }
        }
        if (!foundNamedItem) {
            itemMatch = data.statuses.find(function(item) {
                return abilityText.includes(item.name);
            });
            if (itemMatch) {
                this._setSelectedInArray(data.statuses, itemMatch.id);
                data.selectedStatusId = itemMatch.id;
                data.selectedStatus = itemMatch;
                data.statusLocked = true;
                foundNamedItem = true;
                if (this.ability.system.type !== "A") {
                    data.rollPower = false;
                    data.rollQuality = false;
                }
            }
        }
        
        // set other items
        let selectedItem = null;
        if (!data.selectedPowerId) {
            selectedItem = data.powers.find(function(item) {
                return item.selected;
            });
            data.selectedPowerId = (selectedItem ? selectedItem.id : data.powers[0].id);
            data.selectedPower = (selectedItem ? selectedItem : data.powers[0]);
        }
        if (!data.selectedQualityId) {
            selectedItem = data.qualities.find(function(item) {
                return item.selected;
            });
            data.selectedQualityId = (selectedItem ? selectedItem.id : data.qualities[0].id);
            data.selectedQuality = (selectedItem ? selectedItem : data.qualities[0]);
        }
        if (!data.selectedStatusId) {
            selectedItem = data.statuses.find(function(item) {
                return item.selected;
            });
            data.selectedStatusId = (selectedItem ? selectedItem.id : data.statuses[0].id);
            data.selectedStatus = (selectedItem ? selectedItem : data.statuses[0]);
        }
    }
    
    _setSelectedInArray(array, selectedId) {
        // turn other selecteds off and the match on
        for(var i=0; i < array.length; ++i) {
            let item = array[i];
            item.selected = (item.id === selectedId);
        }
    }

    activateListeners(html) {
        //item select
        html.find(".item-select").click(this._onItemSelect.bind(this));
        //mod select
        html.find(".mod-select").click(this._onModSelect.bind(this));
        //roll set power, quality and status
        html.find(".make-roll").click(this._onMakeRoll.bind(this));

        super.activateListeners(html);
    }

    _onItemSelect(event) {
        event.preventDefault();

        let element = event.currentTarget;
        let itemId = element.value;
        if (itemId === "N/A") {
            let selectType = element.attributes["name"].value;
            // unset the selected and replace with d4
            if (selectType.includes("Power")) {
                this._unsetPower(this.data.selectedPowerId);
                this.data.selectedPowerId = null;
                this.data.selectedPower = null;
            }
            else if (selectType.includes("Quality")) {
                this._unsetQuality(this.data.selectedQualityId);
                this.data.selectedQualityId = null;
                this.data.selectedQuality = null;
            }
            else if (selectType.includes("Status")) {
                this._unsetVillainStatus(this.data.selectedStatusId);
                this.data.selectedStatusId = null;
                this.data.selectedStatus = null;
            }
        }
        else {
            let item = this.actor.items.get(itemId);
            let updatedSelectedValue = !item.system.selected;

            if (item.type === "power") {
                this.data.selectedPowerId = itemId;
            }
            else if (item.type === "quality") {
                this.data.selectedQualityId = itemId;
            }
            else if (item.type === "villainStatus") {
                this.data.selectedStatusId = itemId;
            }

            //Toggle selected state for current
            item.update({ 'system.selected': updatedSelectedValue });
        }
    }

    _onModSelect(event) {
        event.preventDefault();

        let element = event.currentTarget;
        let itemId = element.closest(".item").dataset.itemId;
        let item = this.actor.items.get(itemId);
        let isExclusive = item.system.exclusive;
        let updatedSelectedValue = !item.system.selected;
        let isBonus = parseInt(item.system.value) > 0;  //Check if Bonus or Penality
        let borderClass = isBonus ? "green-border" : "red-border";

        //if exclusive, deselect all other exclusive mods
        if (isExclusive && updatedSelectedValue) {
            let otherExclusives;

            if (isBonus) {
                otherExclusives = this.actor.items.filter(it => it.type == "mod" && it.system.exclusive == true && it.id !== item.id && parseInt(it.system.value) > 0);
            }
            else {
                otherExclusives = this.actor.items.filter(it => it.type == "mod" && it.system.exclusive == true && it.id !== item.id && parseInt(it.system.value) < 0);
            }

            if (otherExclusives.length > 0) {
                //Foreach exclusive, deselect
                otherExclusives.forEach(function(modItem) {
                    modItem.update({ 'system.selected': false });
                    
                    //Change the border since the partial template is not updating?
                    let otherElement = Array.from(element.parentElement.children).find(elem => elem.attributes["data-item-id"].value === modItem.id);
                    if (otherElement) {
                        otherElement.classList.remove(borderClass);
                    }
                });
            }
        }

        //Toggle selected state for current
        item.update({ 'system.selected': updatedSelectedValue });
        
        //Change the border since the partial template is not updating?
        if (updatedSelectedValue) {
             element.classList.add(borderClass);
        }
        else {
             element.classList.remove(borderClass);
        }
    }
    
    _onMakeRoll(event) {
        event.preventDefault();
        
        if (!this.data.rollPower || !this.data.rollQuality || !this.data.rollStatus) {
            // roll one die
            let rollType = null;
            let rollDie = null;
            let rollName = null;
            
            if (this.data.rollPower) {
                rollType = "power";
                var item = this.actor.items.get(this.data.selectedPowerId);
                rollDie = item.system.dieType;
                rollName = item.name;
            }
            else if (this.data.rollQuality) {
                rollType = "quality";
                var item = this.actor.items.get(this.data.selectedQualityId);
                rollDie = item.system.dieType;
                rollName = item.name;
            }
            else if (this.data.rollStatus) {
                rollType = "status";
                if (this.actor.type === "villain") {
                    var item = this.actor.items.get(this.data.selectedStatusId);
                    rollDie = item.system.dieType;
                    rollName = item.name;
                }
                else {
                    rollDie = this.actor.system.thirdDie;
                    rollName = this.actor.system.thirdDieName;
                }
            }
            
            dice.SingleCheck(rollDie, rollType, rollName, this.actor, this.ability);            
        }
        else {
            // roll all dice
            // update the actor's selected power/quality/status with selected values
            if (this.data.rollPower && this.data.selectedPowerId) {
                this._setPower(this.data.selectedPowerId);
            }
            if (this.data.rollQuality && this.data.selectedQualityId) {
                this._setQuality(this.data.selectedQualityId);
            }
            if (this.data.rollStatus && this.data.selectedStatusId) {
                this._setVillainStatus(this.data.selectedStatusId);
            }
        
            dice.TaskCheck(this.actor, this.ability);
        }
        this.close();
    }

    _unsetPower(itemId) {
        let item = this.actor.items.get(itemId);
        this.actor.system.firstDie = "d4";
        this.actor.system.firstDieName = game.i18n.localize("SCRPG.sheet.newItem");
        item.system.selected = null;
        
        this.actor.update({ "system.firstDie": "d4" });
        this.actor.update({ "system.firstDieName": game.i18n.localize("SCRPG.sheet.newItem") });
        item.update({ "system.selected": null });
    }

    _unsetQuality(itemId) {
        let item = this.actor.items.get(itemId);
        this.actor.system.secondDie = "d4";
        this.actor.system.secondDieName = game.i18n.localize("SCRPG.sheet.newItem");
        item.system.selected = null;
        
        this.actor.update({ "system.secondDie": "d4" });
        this.actor.update({ "system.secondDieName": game.i18n.localize("SCRPG.sheet.newItem") });
        item.update({ "system.selected": null });
    }

    _unsetVillainStatus(itemId) {
        if (this.actor.type === "villain") {
            let item = this.actor.items.get(itemId);
            this.actor.system.thirdDie = "d4";
            this.actor.system.thirdDieName = game.i18n.localize("SCRPG.sheet.newItem");
            item.system.selected = null;
            
            this.actor.update({ "system.thirdDie": "d4" });
            this.actor.update({ "system.thirdDieName": game.i18n.localize("SCRPG.sheet.newItem") });
            item.update({ "system.selected": false });
        }
    }

    //Assigns power to main roll
    _setPower(itemId) {
        let item = this.actor.items.get(itemId);
        let otherPowers = [];

        this.actor.system.firstDie = item.system.dieType;
        this.actor.system.firstDieName = item.name;
        item.system.selected = "power";

        this.actor.update({ "system.firstDie": item.system.dieType });
        this.actor.update({ "system.firstDieName": item.name });
        item.update({ "system.selected": "power" });
        if (this.actor.system.civilianMode) {
            otherPowers = this.actor.items.filter(it => (it.type == "quality" && it.key != itemId && it.system.selected != "quality") || it.type == "power");
        } else {
            otherPowers = this.actor.items.filter(it => it.type == "power" && it.key != itemId && it.system.selected != "quality");
        }
        otherPowers.forEach(oe => oe.system.selected = null );
        otherPowers.forEach(oe => oe.update({ 'system.selected': null }));
    }

    //Assigns quality to main roll
    _setQuality(itemId) {
        let item = this.actor.items.get(itemId);
        let otherPowers = [];

        this.actor.system.secondDie = item.system.dieType;
        this.actor.system.secondDieName = item.name;
        item.system.selected = "quality";

        this.actor.update({ "system.secondDie": item.system.dieType });
        this.actor.update({ "system.secondDieName": item.name });
        item.update({ "system.selected": "quality" });
        if (this.actor.system.poweredMode) {
            otherPowers = this.actor.items.filter(it => (it.type == "power" && it.key != itemId && it.system.selected != "power") || it.type == "quality");
        } else {
            otherPowers = this.actor.items.filter(it => it.type == "quality" && it.key != itemId && it.system.selected != "power");
        }
        otherPowers.forEach(oe => oe.system.selected = null );
        otherPowers.forEach(oe => oe.update({ 'system.selected': null }));
    }

    //Assigns villain status to main roll
    _setVillainStatus(itemId) {
        if (this.actor.type === "villain") {
            let item = this.actor.items.get(itemId);
            let otherPowers = [];

            this.actor.system.thirdDie = item.system.dieType;
            this.actor.system.thirdDieName = item.name;
            item.system.selected = true;

            this.actor.update({ "system.thirdDie": item.system.dieType });
            this.actor.update({ "system.thirdDieName": item.name });
            item.update({ "system.selected": true });

            otherPowers = this.actor.items.filter(it => it.type == "villainStatus" && it.key != itemId);
            otherPowers.forEach(oe => oe.system.selected = false );
            otherPowers.forEach(oe => oe.update({ 'system.selected': false }));
        }
    }
}