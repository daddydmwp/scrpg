//Prepares all items associated with the hero/villain sheets
function _prepareCharacterActor(actor) {
    const abilities = [];
    const powers = [];
    const qualities = [];
    const heroMinions = [];
    const minionForms = [];
    const mods = [];
    const villainstatus = [];

    for (let i of actor.items) {
        i.img = i.img;
        if (i.type === "power") {
            powers.push(i);
        }
        else if (i.type === "quality") {
            qualities.push(i);
        }
        else if (i.type === "villainStatus") {
            villainstatus.push(i);
        }
        else if (i.type === "heroMinion") {
            heroMinions.push(i);
        }
        else if (i.type === "minionForm") {
            minionForms.push(i);
        }
        else if (i.type === "mod") {
            mods.push(i);
        }
        else if (i.type === 'ability') {
            abilities.push(i);
        }
    }

    actor.ability = abilities;
    actor.power = powers;
    actor.quality = qualities;
    actor.heroMinion = heroMinions;
    actor.minionForm = minionForms;
    actor.mod = mods;
    actor.villainStatus = villainstatus;
    
    // useful defaults
    if (actor.hasOwnProperty("prototypeToken")) {
        actor.prototypeToken.actorLink = true;
        actor.prototypeToken.bar1.attribute = "wounds";
        actor.prototypeToken.displayBars = 30;
        actor.prototypeToken.displayName = 30;
        actor.update({
          "prototypeToken.actorLink": true,
          "prototypeToken.bar1.attribute": "wounds",
          "prototypeToken.displayBars": 30,
          "prototypeToken.displayName": 30
        });
    }
    else if (actor.hasOwnProperty("token")) {
        actor.token.actorLink = true;
        actor.token.bar1.attribute = "wounds";
        actor.token.displayBars = 30;
        actor.token.displayName = 30;
        actor.update({
          "token.actorLink": true,
          "token.bar1.attribute": "wounds",
          "token.displayBars": 30,
          "token.displayName": 30
        });
    }
}

//Prepares all items associated with the environment sheets
function _prepareEnvironmentActor(actor) {
    const twists = [];
    const minionForms = [];
    const heroMinions = [];

    for (let i of actor.items) {
        i.img = i.img;
        if (i.type === "environmentTwist") {
            twists.push(i);
        }
        else if (i.type === "heroMinion") {
            heroMinions.push(i)
        }
        else if (i.type === "minionForm") {
            minionForms.push(i)
        }
    }

    actor.heroMinion = heroMinions;
    actor.minionForm = minionForms;
    actor.environmentTwist = twists;

    // useful defaults
    if (actor.hasOwnProperty("prototypeToken")) {
        actor.prototypeToken.img = "icons/environment/settlement/city-hall.webp";
        actor.prototypeToken.displayName = 30;
        actor.update({
          "img": "icons/environment/settlement/city-hall.webp",
          "prototypeToken.img": "icons/environment/settlement/city-hall.webp",
          "prototypeToken.displayName": 30
        });
    }
    else if (actor.hasOwnProperty("token")) {
        actor.token.img = "icons/environment/settlement/city-hall.webp";
        actor.token.displayName = 30;
        actor.update({
          "img": "icons/environment/settlement/city-hall.webp",
          "token.img": "icons/environment/settlement/city-hall.webp",
          "token.displayName": 30
        });
    }
}

function _prepareSceneTrackerActor(actor) {
    const inits = [];

    for (let i of actor.items) {
        i.img = i.img;
        if (i.type === "initiativeActor") {
            inits.push(i)
        }
    }

    actor.initiativeActor = inits;

    // useful defaults
    if (actor.hasOwnProperty("prototypeToken")) {
        actor.prototypeToken.img = "icons/svg/explosion.svg";
        actor.prototypeToken.displayName = 30;
        actor.update({
          "img": "icons/svg/explosion.svg",
          "prototypeToken.img": "icons/svg/explosion.svg",
          "prototypeToken.displayName": 30
        });
    }
    else if (actor.hasOwnProperty("token")) {
        actor.token.img = "icons/svg/explosion.svg";
        actor.token.displayName = 30;
        actor.update({
          "img": "icons/svg/explosion.svg",
          "token.img": "icons/svg/explosion.svg",
          "token.displayName": 30
        });
    }
}

function _prepareMinionActor(actor) {
    const heroMinions = [];
    const minionForms = [];
    const mods = [];

    for (let i of actor.items) {
        i.img = i.img;
        if (i.type === "heroMinion") {
            heroMinions.push(i)
        }
        else if (i.type === "minionForm") {
            minionForms.push(i)
        }
        else if (i.type === "mod") {
            mods.push(i);
        }
    }

    actor.heroMinion = heroMinions;
    actor.minionForm = minionForms;

    // useful defaults
    if (actor.hasOwnProperty("prototypeToken")) {
        actor.prototypeToken.actorLink = true;
        actor.prototypeToken.displayName = 30;
        actor.update({
          "prototypeToken.actorLink": true,
          "prototypeToken.displayName": 30
        });
    }
    else if (actor.hasOwnProperty("token")) {
        actor.token.actorLink = true;
        actor.token.displayName = 30;
        actor.update({
          "token.actorLink": true,
          "token.displayName": 30
        });
    }
}

export function prepareActor(actor) {
    if (actor.type == 'hero' || actor.type == 'villain') {
        _prepareCharacterActor(actor);
    } else if (actor.type == 'environment') {
        _prepareEnvironmentActor(actor);
    } else if (actor.type == 'scene') {
        _prepareSceneTrackerActor(actor);
    } else if (actor.type == 'minion') {
        _prepareMinionActor(actor);
    };
}