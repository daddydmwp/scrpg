export function mapValuesToArray(map) {
    let array = [];
    map.forEach(value => array.push(value));
    return array;
}

export function mapKeysToArray(map) {
    let array = [];
    map.forEach(value => array.push(value.id));
    return array;
}
