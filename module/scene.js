import * as util from "./util.js";

export function getCurrentScene() {
    let scenes = util.mapValuesToArray(game.scenes);
    let current = scenes.find(sc => sc.isView);
    return current ? current : null;
}

export function getTokensInScene(scene) {
    return util.mapValuesToArray(scene.tokens);
}

export function getActorsFromTokens(tokens) {
    return tokens.map(tk => tk.actor);
}

export async function SceneReset(actor = null) {
    actor.update({ "system.redSpace.current": 0 });
    actor.update({ "system.yellowSpace.current": 0 });
    actor.update({ "system.greenSpace.current": 0 });
};

export async function SetGreen() {
    let scene = getCurrentScene();
    let tokens = getTokensInScene(scene);
    let actors = getActorsFromTokens(tokens);
    for (let i = 0; i < actors.length; i++) {
        if (actors[i].type == "hero") {
            let health = actors[i].system.wounds.value;
            let redHigh = actors[i].system.health.redHigh;
            let yellowHigh = actors[i].system.health.yellowHigh;
            actors[i].update({ "system.scene": "green" });
            if (health <= redHigh) {
                actors[i].update({ "system.thirdDieName": "red" });
                if (actors[i].system.civilianMode == true) {
                    actors[i].update({ "system.thirdDie": actors[i].system.statusDie.civilianRed });
                } else {
                    actors[i].update({ "system.thirdDie": actors[i].system.statusDie.red });
                };
            } else if (health <= yellowHigh) {
                actors[i].update({ "system.thirdDieName": "yellow" });
                if (actors[i].system.civilianMode == true) {
                    actors[i].update({ "system.thirdDie": actors[i].system.statusDie.civilianYellow });
                } else {
                    actors[i].update({ "system.thirdDie": actors[i].system.statusDie.yellow });
                };
            } else {
                actors[i].update({ "system.thirdDieName": "green" });
                if (actors[i].system.civilianMode == true) {
                    actors[i].update({ "system.thirdDie": actors[i].system.statusDie.civilianGreen });
                } else {
                    actors[i].update({ "system.thirdDie": actors[i].system.statusDie.green });
                };
            };
        };
    };
};

export async function SetYellow() {
    let scene = getCurrentScene();
    let tokens = getTokensInScene(scene);
    let actors = getActorsFromTokens(tokens);
    for (let i = 0; i < actors.length; i++) {
        if (actors[i].type == "hero") {
            let health = actors[i].system.wounds.value;
            let redHigh = actors[i].system.health.redHigh;
            actors[i].update({ "system.scene": "yellow" });
            if (health <= redHigh) {
                actors[i].update({ "system.thirdDieName": "red" });
                if (actors[i].system.civilianMode == true) {
                    actors[i].update({ "system.thirdDie": actors[i].system.statusDie.civilianRed });
                } else {
                    actors[i].update({ "system.thirdDie": actors[i].system.statusDie.red });
                };
            } else {
                actors[i].update({ "system.thirdDieName": "yellow" });
                if (actors[i].system.civilianMode == true) {
                    actors[i].update({ "system.thirdDie": actors[i].system.statusDie.civilianYellow });
                } else {
                    actors[i].update({ "system.thirdDie": actors[i].system.statusDie.yellow });
                };
            };
        };
    };
};

export async function SetRed() {
    let scene = getCurrentScene();
    let tokens = getTokensInScene(scene);
    let actors = getActorsFromTokens(tokens);
    for (let i = 0; i < actors.length; i++) {
        if (actors[i].type == "hero") {
            actors[i].update({ "system.scene": "red" });
            if (actors[i].system.civilianMode == true) {
                actors[i].update({ "system.thirdDie": actors[i].system.statusDie.civilianRed });
            } else {
                actors[i].update({ "system.thirdDie": actors[i].system.statusDie.red });
            };
            actors[i].update({ "system.thirdDieName": "red" });
        };
    };
};

export async function SceneChat(type = null) {
    let coloring = ""

    if (type == "reset" || type == "failure") {
        coloring = "villain"
    } else {
        coloring = type
    }

    let chatData = {
        coloring: coloring,
        type: type
    }
    const messageTemplate = "systems/scrpg/templates/chat/sceneupdate.hbs";

    let render = await renderTemplate(messageTemplate, chatData)

    let messageData = {
        speaker: ChatMessage.getSpeaker(),
        content: render,
    };

    return ChatMessage.create(messageData);
};